import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class MinDka {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String states = reader.readLine();
		String alphabet = reader.readLine();
		String acceptableStates = reader.readLine();
		String initialState = reader.readLine();

		List<String> transitions = new ArrayList<>();

		String t = reader.readLine();
		while (t != null && !t.trim().isEmpty()) {
			transitions.add(t);
			t = reader.readLine();
		}

		String[] statesQ = states.split(",");

		String[] alphabetS = alphabet.split(",");

		String[] acceptableStatesQ = acceptableStates.split(",");

		String initialStateQ = initialState;

		if (acceptableStatesQ.length == 1 && acceptableStatesQ[0].isEmpty())
			acceptableStatesQ = new String[0];

		List<String> acceptables = Arrays.asList(acceptableStatesQ);
		List<TableRow> rows = makeTable(transitions, acceptables);

		Set<String> reachableStates = getReachableStatesSet(initialStateQ, statesQ, rows, alphabetS);

		rows = rows.stream().filter(row -> reachableStates.containsAll(row.getCurrentState()))
				.collect(Collectors.toList());

		List<List<String>> listaListaGrupaNula = new ArrayList<>(
				rows.stream().filter(r -> r.acceptable == 0).map(r -> r.currentState).collect(Collectors.toSet()));
		Set<String> setGrupaNula = new TreeSet<>();
		listaListaGrupaNula.forEach(list -> setGrupaNula.addAll(list));
		List<List<String>> listaListaGrupaJedan = new ArrayList<>(
				rows.stream().filter(r -> r.acceptable == 1).map(r -> r.currentState).collect(Collectors.toSet()));
		Set<String> setGrupaJedan = new TreeSet<>();
		listaListaGrupaJedan.forEach(list -> setGrupaJedan.addAll(list));
		List<Set<String>> listaGrupa = new ArrayList<>();
		listaGrupa.add(setGrupaNula);
		listaGrupa.add(setGrupaJedan);

		boolean recursiveIndicator = true;
		while (recursiveIndicator) {
			recursiveIndicator = false;

			List<Set<String>> listaNovihGrupa = new ArrayList<>();

			for (int i = 0, n = listaGrupa.size(); i < n; i++) {
				Set<String> newGroup = new TreeSet<>();
				Set<String> setStates = listaGrupa.get(i);
				if (setStates.size() == 0)
					continue;
				List<String> lstStats = new ArrayList<>();
				lstStats.addAll(setStates);
				String firstElGroup = lstStats.get(0);
				for (int j = 0, m = lstStats.size(); j < m; j++) {
					if (j == 0)
						continue;
					String state = lstStats.get(j);
					for (String al : alphabetS) {
						int grIndSt = findGroupIndex(listaGrupa, rows, state, al);
						int grIndStartEl = findGroupIndex(listaGrupa, rows, firstElGroup, al);
						if (grIndSt != grIndStartEl) {
							setStates.remove(state);
							newGroup.add(state);

							recursiveIndicator = true;
							break;
						}
					}
				}
				listaNovihGrupa.add(newGroup);

			}

			listaGrupa.addAll(listaNovihGrupa);
		}

		listaGrupa = listaGrupa.stream().filter(set -> set.size() != 0).collect(Collectors.toList());

		for (TableRow row : rows) {
			String curSt = row.currentState.get(0);
			String nexSt = row.nextState.get(0);
			row.currentState = findGroupFromListGroup(listaGrupa, curSt);
			row.nextState = findGroupFromListGroup(listaGrupa, nexSt);
		}

		rows = rows.stream().distinct().collect(Collectors.toList());

		rows.forEach(row -> {
			List<String> curSts = row.currentState;
			List<String> nexSts = row.nextState;
			List<String> newCurSts = new ArrayList<>();
			newCurSts.add(curSts.get(0));
			row.currentState = newCurSts;
			List<String> newNexSts = new ArrayList<>();
			newNexSts.add(nexSts.get(0));
			row.nextState = newNexSts;
		});

		Set<String> setNewStates = new TreeSet<>();
		setNewStates.addAll(rows.stream().map(row -> row.getCurrentState().get(0)).collect(Collectors.toSet()));
		List<String> lst = new ArrayList<>();
		lst.addAll(setNewStates);

		String newStStr = "";
		for (int i = 0; i < lst.size() - 1; i++) {
			newStStr += lst.get(i) + ",";
		}

		System.out.println(newStStr + lst.get(lst.size() - 1));

		System.out.println(alphabet);

		if (acceptableStatesQ.length == 0) {
			System.out.println();
		} else {
			Set<String> setAcceptableStates = new TreeSet<>();
			setAcceptableStates.addAll(rows.stream().filter(row -> row.isAcceptable() == 1)
					.map(row -> row.getCurrentState().get(0)).collect(Collectors.toSet()));
			List<String> lstAcc = new ArrayList<>();
			lstAcc.addAll(setAcceptableStates);

			if (lstAcc.size() == 0) {
				System.out.println();
			} else {
				String newAcceptableStStr = "";
				for (int i = 0; i < lstAcc.size() - 1; i++) {
					newAcceptableStStr += lstAcc.get(i) + ",";
				}
				System.out.println(newAcceptableStStr + lstAcc.get(lstAcc.size() - 1));
			}
		}

		String initStatePrint = "";

		for (Set<String> grupaG : listaGrupa) {
			if (grupaG.contains(initialStateQ)) {
				initStatePrint = new ArrayList<>(grupaG).get(0);
			}
		}

		System.out.println(initStatePrint);

		rows.forEach(row -> {
			String curState = row.currentState.get(0).trim();

			String nexState = row.nextState.get(0).trim();
			System.out.println(curState + "," + row.inChar + "->" + nexState);

		});

		reader.close();
	}

	private static List<String> findGroupFromListGroup(List<Set<String>> listaGrupa, String st) {
		Set<String> setRet = new TreeSet<>();
		for (Set<String> set : listaGrupa) {
			if (set.contains(st)) {
				setRet = new TreeSet<>(set);
				break;
			}
		}
		return new ArrayList<>(setRet);
	}

	private static int findGroupIndex(List<Set<String>> listaGrupa, List<TableRow> rows, String state, String al) {
		for (TableRow row : rows) {
			if (row.currentState.contains(state) && row.inChar.equals(al)) {
				String ns = row.nextState.get(0);
				for (int i = 0, n = listaGrupa.size(); i < n; i++) {
					Set<String> set = listaGrupa.get(i);
					if (set.contains(ns)) {
						return i;
					}
				}
			}
		}
		return -1;
	}

	private static Set<String> getReachableStatesSet(String initialStateQ, String[] statesQ, List<TableRow> rows,
			String[] alphabetS) {
		List<String> states = new ArrayList<>();
		int currentInd = 0;
		states.add(initialStateQ);
		String currentState = initialStateQ;

		while (currentInd < states.size()) {
			Set<String> newStates = new LinkedHashSet<>();
			newStates.addAll(states);

			for (String ch : alphabetS) {
				List<TableRow> rowsFiltered = new ArrayList<>();
				for (TableRow r : rows) {
					if (r.getCurrentState().contains(currentState) && r.getInChar().equals(ch)) {
						rowsFiltered.add(r);
					}
				}

				List<List<String>> rowsNewStrs = rowsFiltered.stream().map(row -> row.getNextState())
						.collect(Collectors.toList());
				List<String> rowsNew = new ArrayList<>();
				for (List<String> rownew : rowsNewStrs) {
					rowsNew.addAll(rownew);
				}
				rowsNew = rowsNew.stream().collect(Collectors.toList());
				newStates.addAll(rowsNew);
			}
			if (newStates.size() != states.size()) {
				for (String s : newStates) {
					if (!states.contains(s)) {
						states.add(s);
					}
				}

			}
			currentInd++;
			if (currentInd == states.size())
				break;
			currentState = states.get(currentInd);
		}
		Set<String> statesRet = new TreeSet<>(states);
		return statesRet;
	}

	private static List<TableRow> makeTable(List<String> transitions, List<String> acceptables) {
		List<TableRow> tableRows = new ArrayList<>();
		for (String t : transitions) {
			String[] lr = t.split("->");
			String[] l = lr[0].split(",");
			String currentQ = l[0];
			String currentCh = l[1];
			String newState = lr[1];
			byte acc = (byte) (acceptables.contains(currentQ) ? 1 : 0);
			TableRow row = new TableRow(Arrays.asList(currentQ), currentCh, Arrays.asList(newState), acc);

			tableRows.add(row);
		}

		return tableRows;

	}

	private static class TableRow {
		private List<String> currentState;
		private String inChar;
		private List<String> nextState;
		private byte acceptable;

		public TableRow(List<String> currentState, String inChar, List<String> nextState, byte acceptable) {
			super();
			this.currentState = currentState;
			this.inChar = inChar;
			this.nextState = nextState;
			this.acceptable = acceptable;
		}

		public List<String> getCurrentState() {
			return currentState;
		}

		public String getInChar() {
			return inChar;
		}

		public List<String> getNextState() {
			return nextState;
		}

		public byte isAcceptable() {
			return acceptable;
		}

		@Override
		public int hashCode() {
			return Objects.hash(acceptable, currentState, inChar, nextState);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof TableRow))
				return false;
			TableRow other = (TableRow) obj;
			return acceptable == other.acceptable && Objects.equals(currentState, other.currentState)
					&& Objects.equals(inChar, other.inChar) && Objects.equals(nextState, other.nextState);
		}

		@Override
		public String toString() {
			return "TableRow [currentState=" + currentState + ", inChar=" + inChar + ", nextState=" + nextState
					+ ", acceptable=" + acceptable + "]";
		}

	}

}
